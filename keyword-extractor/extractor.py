import os
import collections

with open('files/stopwords.txt', 'r') as stopWordsFile:
    stopwords = stopWordsFile.read().splitlines()

    count = {};

    for file in os.listdir("files"):
        if file.endswith(".md"):
            mdFile = open('files/' + file)
            words = mdFile.read()

            for word in words.lower().split():
                word = word.replace(".","")
                word = word.replace(",","")
                word = word.replace(":","")
                word = word.replace("\"","")
                word = word.replace("!","")
                word = word.replace("*","")

                if word not in stopwords:
                    if word not in count:
                        count[word] = 1
                    else:
                        count[word] += 1

    wordCounter = collections.Counter(count)
    for word, count in wordCounter.most_common(10):
        print(word, ": ", count)
