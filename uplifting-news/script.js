function fetchFeeds() {
  var feedUrl = 'https://www.reddit.com/r/UpliftingNews/.json';
  var request = new XMLHttpRequest();

  request.open('GET', feedUrl);
  request.responseType = 'json';
  request.send();
  request.onload = function() {
    cacheFeeds(request.response);
  }
}

function cacheFeeds(feedList) {
  window.localStorage.setItem('feeds', JSON.stringify(feedList));
}

function getFeedsFromCache() {
  return window.localStorage.getItem('feeds');
}

function feedsCached()
{
  return window.localStorage.getItem('feeds') != null;
}

window.onload = function () {
  var feeds = feedsCached() ? getFeedsFromCache() : fetchFeeds();
  feeds = JSON.parse(feeds);

  var children = feeds.data.children;

  for (var key in children) {
    if (children.hasOwnProperty(key)) {
      if (!children[key].data.visited || !children[key].data.clicked) {
        document.write('<a href="'+children[key].data.url+'">'+children[key].data.title+'</a>'+'<br>');
      }
    }
  }
};
